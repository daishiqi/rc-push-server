<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: query_message_status_response.proto

namespace Query;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>query.QueryMessageStatusResponseDataReqPushRecord</code>
 */
class QueryMessageStatusResponseDataReqPushRecord extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>int32 third_push_id = 1;</code>
     */
    protected $third_push_id = 0;
    /**
     * Generated from protobuf field <code>int32 type = 2;</code>
     */
    protected $type = 0;
    /**
     * Generated from protobuf field <code>int32 sync_type = 3;</code>
     */
    protected $sync_type = 0;
    /**
     * Generated from protobuf field <code>string platform = 4;</code>
     */
    protected $platform = '';
    /**
     * Generated from protobuf field <code>string audience = 5;</code>
     */
    protected $audience = '';
    /**
     * Generated from protobuf field <code>string send_id = 6;</code>
     */
    protected $send_id = '';
    /**
     * Generated from protobuf field <code>string message = 7;</code>
     */
    protected $message = '';
    /**
     * Generated from protobuf field <code>string notification = 8;</code>
     */
    protected $notification = '';
    /**
     * Generated from protobuf field <code>string serial_no = 9;</code>
     */
    protected $serial_no = '';

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type int $third_push_id
     *     @type int $type
     *     @type int $sync_type
     *     @type string $platform
     *     @type string $audience
     *     @type string $send_id
     *     @type string $message
     *     @type string $notification
     *     @type string $serial_no
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\QueryMessageStatusResponse::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>int32 third_push_id = 1;</code>
     * @return int
     */
    public function getThirdPushId()
    {
        return $this->third_push_id;
    }

    /**
     * Generated from protobuf field <code>int32 third_push_id = 1;</code>
     * @param int $var
     * @return $this
     */
    public function setThirdPushId($var)
    {
        GPBUtil::checkInt32($var);
        $this->third_push_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 type = 2;</code>
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Generated from protobuf field <code>int32 type = 2;</code>
     * @param int $var
     * @return $this
     */
    public function setType($var)
    {
        GPBUtil::checkInt32($var);
        $this->type = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>int32 sync_type = 3;</code>
     * @return int
     */
    public function getSyncType()
    {
        return $this->sync_type;
    }

    /**
     * Generated from protobuf field <code>int32 sync_type = 3;</code>
     * @param int $var
     * @return $this
     */
    public function setSyncType($var)
    {
        GPBUtil::checkInt32($var);
        $this->sync_type = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string platform = 4;</code>
     * @return string
     */
    public function getPlatform()
    {
        return $this->platform;
    }

    /**
     * Generated from protobuf field <code>string platform = 4;</code>
     * @param string $var
     * @return $this
     */
    public function setPlatform($var)
    {
        GPBUtil::checkString($var, True);
        $this->platform = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string audience = 5;</code>
     * @return string
     */
    public function getAudience()
    {
        return $this->audience;
    }

    /**
     * Generated from protobuf field <code>string audience = 5;</code>
     * @param string $var
     * @return $this
     */
    public function setAudience($var)
    {
        GPBUtil::checkString($var, True);
        $this->audience = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string send_id = 6;</code>
     * @return string
     */
    public function getSendId()
    {
        return $this->send_id;
    }

    /**
     * Generated from protobuf field <code>string send_id = 6;</code>
     * @param string $var
     * @return $this
     */
    public function setSendId($var)
    {
        GPBUtil::checkString($var, True);
        $this->send_id = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string message = 7;</code>
     * @return string
     */
    public function getMessage()
    {
        return $this->message;
    }

    /**
     * Generated from protobuf field <code>string message = 7;</code>
     * @param string $var
     * @return $this
     */
    public function setMessage($var)
    {
        GPBUtil::checkString($var, True);
        $this->message = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string notification = 8;</code>
     * @return string
     */
    public function getNotification()
    {
        return $this->notification;
    }

    /**
     * Generated from protobuf field <code>string notification = 8;</code>
     * @param string $var
     * @return $this
     */
    public function setNotification($var)
    {
        GPBUtil::checkString($var, True);
        $this->notification = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>string serial_no = 9;</code>
     * @return string
     */
    public function getSerialNo()
    {
        return $this->serial_no;
    }

    /**
     * Generated from protobuf field <code>string serial_no = 9;</code>
     * @param string $var
     * @return $this
     */
    public function setSerialNo($var)
    {
        GPBUtil::checkString($var, True);
        $this->serial_no = $var;

        return $this;
    }

}

