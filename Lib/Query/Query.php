<?php
/**
 * 查询模块
 */
namespace App\Lib\RcPushServer\Lib\Query;

use App\Lib\RcPushServer\Lib\Request;
use App\Lib\RcPushServer\Lib\Utils;
use Query\QueryMessageStatusRequest;
use Query\QueryMessageStatusResponse;

class Query
{
    /**
     * 推送模块路径
     *
     * @var string
     */
    private $jsonPath = 'Lib/Query/';

    /**
     * 请求配置文件
     *
     * @var string
     */
    private $conf = "";

    /**
     * 校验配置文件
     *
     * @var string
     */
    private $verify = "";

    /**
     * Push constructor.
     */
    function __construct()
    {
        $this->conf = Utils::getJson($this->jsonPath.'api.json');
        $this->verify = Utils::getJson($this->jsonPath.'verify.json');
    }

    /**
     * 查询
     *
     * @param $query array query 参数
     * @param
     * $query = [
             'query_serial_no'=> '', //广播push发送流水号
        ];
     * @return array
     */
    public function query(array $query = []){
        $conf = $this->conf['query'];
        $error = (new Utils())->check([
                    'api'=> $conf,
                    'model'=> 'query',
                    'data'=> $query,
                    'verify'=> $this->verify['query']
                ]);
        if($error) return $error;


        $utils = new Utils();
        //数组转成proto 对象
        $proto = $utils->arrayToProto(new QueryMessageStatusRequest(), $query,'Query\\QueryMessageStatusRequest');

        //发起请求
        $result = (new Request())->GuzzleRequest($conf['url'], $proto);

        //proto字符串->protocol对象->数组
        $class = new QueryMessageStatusResponse();
        $class->mergeFromString($result);

        $protoArray = $utils->protoToArray($class);

        $result = (new Utils())->responseError($protoArray, $conf['response']['fail']);
        return $result;
    }





}