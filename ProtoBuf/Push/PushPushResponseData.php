<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: push_push_response.proto

namespace Push;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>push.PushPushResponseData</code>
 */
class PushPushResponseData extends \Google\Protobuf\Internal\Message
{
    /**
     *RTC请求响应日志
     *
     * Generated from protobuf field <code>repeated .push.PushPushResponseDataPushLog push_log = 1;</code>
     */
    private $push_log;
    /**
     *第三方请求日志
     *
     * Generated from protobuf field <code>repeated .push.PushPushResponseDataReqPushRecord req_push_record = 2;</code>
     */
    private $req_push_record;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type \Push\PushPushResponseDataPushLog[]|\Google\Protobuf\Internal\RepeatedField $push_log
     *          RTC请求响应日志
     *     @type \Push\PushPushResponseDataReqPushRecord[]|\Google\Protobuf\Internal\RepeatedField $req_push_record
     *          第三方请求日志
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\PushPushResponse::initOnce();
        parent::__construct($data);
    }

    /**
     *RTC请求响应日志
     *
     * Generated from protobuf field <code>repeated .push.PushPushResponseDataPushLog push_log = 1;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getPushLog()
    {
        return $this->push_log;
    }

    /**
     *RTC请求响应日志
     *
     * Generated from protobuf field <code>repeated .push.PushPushResponseDataPushLog push_log = 1;</code>
     * @param \Push\PushPushResponseDataPushLog[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setPushLog($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Push\PushPushResponseDataPushLog::class);
        $this->push_log = $arr;

        return $this;
    }

    /**
     *第三方请求日志
     *
     * Generated from protobuf field <code>repeated .push.PushPushResponseDataReqPushRecord req_push_record = 2;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getReqPushRecord()
    {
        return $this->req_push_record;
    }

    /**
     *第三方请求日志
     *
     * Generated from protobuf field <code>repeated .push.PushPushResponseDataReqPushRecord req_push_record = 2;</code>
     * @param \Push\PushPushResponseDataReqPushRecord[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setReqPushRecord($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::MESSAGE, \Push\PushPushResponseDataReqPushRecord::class);
        $this->req_push_record = $arr;

        return $this;
    }

}

