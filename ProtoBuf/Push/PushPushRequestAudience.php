<?php
# Generated by the protocol buffer compiler.  DO NOT EDIT!
# source: push_push_request.proto

namespace Push;

use Google\Protobuf\Internal\GPBType;
use Google\Protobuf\Internal\RepeatedField;
use Google\Protobuf\Internal\GPBUtil;

/**
 * Generated from protobuf message <code>Push.PushPushRequestAudience</code>
 */
class PushPushRequestAudience extends \Google\Protobuf\Internal\Message
{
    /**
     * Generated from protobuf field <code>bool is_to_all = 1;</code>
     */
    protected $is_to_all = false;
    /**
     * Generated from protobuf field <code>repeated int32 userid = 2;</code>
     */
    private $userid;
    /**
     * Generated from protobuf field <code>repeated string tag = 3;</code>
     */
    private $tag;
    /**
     * Generated from protobuf field <code>repeated string tag_or = 4;</code>
     */
    private $tag_or;

    /**
     * Constructor.
     *
     * @param array $data {
     *     Optional. Data for populating the Message object.
     *
     *     @type bool $is_to_all
     *     @type int[]|\Google\Protobuf\Internal\RepeatedField $userid
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $tag
     *     @type string[]|\Google\Protobuf\Internal\RepeatedField $tag_or
     * }
     */
    public function __construct($data = NULL) {
        \GPBMetadata\PushPushRequest::initOnce();
        parent::__construct($data);
    }

    /**
     * Generated from protobuf field <code>bool is_to_all = 1;</code>
     * @return bool
     */
    public function getIsToAll()
    {
        return $this->is_to_all;
    }

    /**
     * Generated from protobuf field <code>bool is_to_all = 1;</code>
     * @param bool $var
     * @return $this
     */
    public function setIsToAll($var)
    {
        GPBUtil::checkBool($var);
        $this->is_to_all = $var;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated int32 userid = 2;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Generated from protobuf field <code>repeated int32 userid = 2;</code>
     * @param int[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setUserid($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::INT32);
        $this->userid = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated string tag = 3;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Generated from protobuf field <code>repeated string tag = 3;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setTag($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->tag = $arr;

        return $this;
    }

    /**
     * Generated from protobuf field <code>repeated string tag_or = 4;</code>
     * @return \Google\Protobuf\Internal\RepeatedField
     */
    public function getTagOr()
    {
        return $this->tag_or;
    }

    /**
     * Generated from protobuf field <code>repeated string tag_or = 4;</code>
     * @param string[]|\Google\Protobuf\Internal\RepeatedField $var
     * @return $this
     */
    public function setTagOr($var)
    {
        $arr = GPBUtil::checkRepeatedField($var, \Google\Protobuf\Internal\GPBType::STRING);
        $this->tag_or = $arr;

        return $this;
    }

}

